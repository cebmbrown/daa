# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  btnDisplay = $('#btnRange')
  btnMark = $('#btnMark')
  btnDisplay.click ->
    alert window.getSelection().getRangeAt(0)
    false
  btnMark.click ->
    range = window.getSelection().getRangeAt(0)
    newNode = document.createElement('mark')
    range.surroundContents newNode
    false
