class DocumentMarker < ActiveRecord::Base

    belongs_to :document
    belongs_to :source

    validates :document, presence: true
    validates :source, presence: true
    validates :position, presence: true, length: { maximum: 100 }

    def attach_to_document(args)
        self.document = Document.find(args[:document_id])
    end

    def attach_to_source(args)
        self.source = Source.find_or_create_by(name: args[:name])
    end

end
