class Document < ActiveRecord::Base

    belongs_to :document_store

    has_many :document_markers

    validates :document_store, presence: true
    validates :name, presence: true, length: { maximum: 50 }

    mount_uploader :file, FileUploader

    # Convert to HTML after saving document.
    after_save :convertPdf2Html

    def html_absolute_path
        p = Pathname.new(self.file.path)
        File.join( p.dirname, p.basename("pdf") ) + "html"
    end

    # Relative path from app directory.
    def html_relative_path
        p = Pathname.new(self.file.to_s)
        File.join( 'public', p.dirname, p.basename("pdf") ) + "html"
    end

    private
    def convertPdf2Html
        Rails.logger.info "Document::convertPDF2HTML"

        in_path  = self.file.path
        out_path = self.html_relative_path  # Kristin gem requires this
                                            # relative path for the outfile.

        if File.exists?(in_path)
            ConvertPdfToHtmlJob.perform_later(in_path, out_path)
        end
    end
end
