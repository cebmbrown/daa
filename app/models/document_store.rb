class DocumentStore < ActiveRecord::Base

  before_validation(on: :create) { generate_access_token }

  belongs_to :user

  has_many :documents, dependent: :destroy

  validates :user, presence: true
  validates :name, presence: true, length: { maximum: 50 }
  validates :access_token, presence: true

  private

  def generate_access_token
    self.access_token = loop do
      random_token = SecureRandom.hex
      break random_token unless self.class.exists?(access_token: random_token)
    end
  end
end
