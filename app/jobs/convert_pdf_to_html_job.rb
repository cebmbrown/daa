class ConvertPdfToHtmlJob < ActiveJob::Base
  queue_as :default

  def perform(*args)
    input_path  = args[0]
    output_path = args[1]

    Rails.logger.info "Document::convert2HTML"

    # dirname should be full path without file. For example:
    # File.dirname("/dir/to/file/sample.pdf") = "/dir/to/file"
    # dirname = File.dirname(input_path)

    # basename should be without extension.
    # File.basename("/dir/to/file/sample.pdf", ".pdf") = "sample"
    # basename = File.basename(input_path, ".pdf")

    # Uses pdftohtml.
    # cmd = "/usr/bin/pdftohtml -c -q #{input_path}"

    # Uses pdf2htmlEX.
    # cmd = "/usr/bin/pdf2htmlEX --dest-dir #{dirname} --optimize-text 1 --correct-text-visibility 1 #{input_path}"

    # Execute shell command.
    # `#{cmd}`

    # Uses Kristin gem.
    Kristin.convert(input_path, output_path)
    Rails.logger.info "Document::convert2HTML - PDF to HTML conversion exit with status code: " + $?.to_s

    # After conversion we submit job for html cleanup.
    #TidyHtmlJob.perform_later output_path
  end
end
