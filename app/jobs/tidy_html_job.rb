require 'fileutils'

class TidyHtmlJob < ActiveJob::Base
  queue_as :default

  def perform(*args)
    # Save the location of the html file.
    file_name = args[0]

    text = File.read(file_name)
    new_contents = text.gsub(/<[\/]?span\s*.*?>/, "")

    # To merely print the contents of the file, use:
    # puts new_contents

    begin
      # To write changes to the file, use:
      File.open(file_name, "w") {|file| file.puts new_contents }
    rescue Exception => e
      #!!! If something goes wrong it should be emailed to devs.
      File.open(file_name, "w") {|file| file.puts text }
    end
  end
end
