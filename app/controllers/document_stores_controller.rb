class DocumentStoresController < ApplicationController
  before_action :logged_in_user
  before_action :set_user,           only: [:index, :new, :create]
  before_action :set_document_store, only: [:show, :edit, :update, :destroy]
  before_action :correct_user,       only: [:show, :edit, :update, :destroy]

  # GET /users/:user_id/document_stores
  # GET /users/:user_id/document_stores.json
  def index
    @document_stores = @user.document_stores
  end

  # GET /document_stores/1
  # GET /document_stores/1.json
  def show
    @documents = @document_store.documents
  end

  # GET /users/:user_id/document_stores/new
  def new
    @document_store = @user.document_stores.build
  end

  # GET /document_stores/1/edit
  def edit
  end

  # POST /users/:user_id/document_stores
  # POST /users/:user_id/document_stores.json
  def create
    @document_store = @user.document_stores.build(document_store_params)

    respond_to do |format|
      if @document_store.save
        format.html { redirect_to @document_store, notice: 'Document store was successfully created.' }
        format.json { render :show, status: :created, location: @document_store }
      else
        format.html { render :new }
        format.json { render json: @document_store.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /document_stores/1
  # PATCH/PUT /document_stores/1.json
  def update
    respond_to do |format|
      if @document_store.update(document_store_params)
        format.html { redirect_to @document_store, notice: 'Document store was successfully updated.' }
        format.json { render :show, status: :ok, location: @document_store }
      else
        format.html { render :edit }
        format.json { render json: @document_store.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /document_stores/1
  # DELETE /document_stores/1.json
  def destroy
    @document_store.destroy
    respond_to do |format|
      format.html { redirect_to user_document_stores_url(@document_store.user_id), notice: 'Document store was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    # Before actions

    # Use callbacks to share common setup or constraints between actions.
    def set_document_store
      @document_store = DocumentStore.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def document_store_params
      params.require(:document_store).permit(:user_id, :name)
    end

    def set_user
      @user = User.find(params[:user_id])
    end

    def correct_user
      @document_store = current_user.document_stores.find_by(id: params[:id])
      if @document_store.nil?
        flash[:danger] = "You do not have access to the requested resource."
        redirect_to root_url
      end
    end
end
