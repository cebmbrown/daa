module API
  class V1::DocumentStoresController < ApplicationController
    before_action :restrict_access

    def index
      respond_to do |format|
        format.json { @document_stores }
      end
    end

    private

    def restrict_access
      @lsof_access_tokens = params[:access_token].split(",")
      @document_stores    = DocumentStore.where(access_token: @lsof_access_tokens)
      head :unauthorized unless @document_stores
    end
  end
end
