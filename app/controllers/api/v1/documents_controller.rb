module API
    class V1::DocumentsController < ApplicationController

        def index
            respond_to do |format|
                format.json { @documents = DocumentStore.find(params[:document_store_id]).documents }
            end
        end

        def html
            render body: File.read(Document.find(params[:document_id]).html_absolute_path)
        end

    end
end
