module API
    class V1::DocumentMarkersController < ApplicationController

        skip_before_action :verify_authenticity_token, only: :create

        def index
            respond_to do |format|
                format.json { @document_markers = Document.find(params[:document_id]).document_markers }
            end
        end

        def show
            respond_to do |format|
                format.json { @document_marker = DocumentMarker.find(params[:id]) }
            end
        end

        def create
            @document_marker = DocumentMarker.new(document_marker_params)
            @document_marker.attach_to_document(document_id: params[:document_id])
            @document_marker.attach_to_source(name: @_request.remote_host)
            respond_to do |format|
                if @document_marker.save
                    format.json { @document_marker }
                else
                    format.json { render json: { errors: @document_marker.errors.full_messages }, status: 422 }
                end
            end
        end

        private
        def document_marker_params
            params.require(:document_marker).permit(:text, :position)
        end

    end
end
