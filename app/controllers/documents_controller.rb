class DocumentsController < ApplicationController
  #before_action :logged_in_user
  #before_action :admin_user,         only: [:edit, :destroy]
  before_action :set_document_store, only: [:index, :new, :create]
  before_action :set_document,       only: [:show, :edit, :update, :destroy]
  before_action :correct_user,       only: [:show, :edit, :update, :destroy]

  # GET /document_stores/:document_store_id/documents
  # GET /document_stores/:document_store_id/documents.json
  def index
    @documents = @document_store.documents
  end

  # GET /documents/1
  # GET /documents/1.json
  def show
  end

  # GET /document_stores/:document_store_id/documents/new
  def new
    @document = @document_store.documents.build
  end

  # GET /documents/1/edit
  def edit
  end

  # POST /document_stores/:document_store_id/documents
  # POST /document_stores/:document_store_id/documents.json
  def create
    @document = @document_store.documents.build(document_params)

    respond_to do |format|
      if @document.save
        flash[:success] = 'Document was successfully created.'
        format.html { redirect_to @document }
        format.json { render :show, status: :created, location: @document }
      else
        format.html { render :new }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /documents/1
  # PATCH/PUT /documents/1.json
  def update
    respond_to do |format|
      if @document.update(document_params)
        flash[:success] = 'Document was successfully updated.'
        format.html { redirect_to @document }
        format.json { render :show, status: :ok, location: @document }
      else
        format.html { render :edit }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/1
  # DELETE /documents/1.json
  def destroy
    @document.destroy
    respond_to do |format|
      flash[:success] = 'Document was successfully destroyed.'
      format.html { redirect_to document_store_documents_url(@document.document_store) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_document
      @document = Document.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def document_params
      params.require(:document).permit(:document_store_id, :name, :description, :file)
    end

    def set_document_store
      @document_store = DocumentStore.find(params[:document_store_id])
    end

    def correct_user
      @document = current_user.documents.find_by(id: params[:id])
      if @document.nil?
        flash[:danger] = "You do not have access to the requested resource."
        redirect_to root_url
      end
    end
end
