json.array!(@document_markers) do |document_marker|
    json.extract! document_marker, :id, :text, :position
    json.url api_v1_document_marker_url(document_marker, format: :json)
end
