json.extract! @document_marker, :id, :text, :position, :document
json.url api_v1_document_marker_url(@document_marker, format: :json)
