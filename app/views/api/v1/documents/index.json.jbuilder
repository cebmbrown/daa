json.array!(@documents) do |document|
    json.extract! document, :id, :name, :description
    json.document_marker_count document.document_markers.count
end
