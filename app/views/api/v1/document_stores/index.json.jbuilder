json.array!(@document_stores) do |document_store|
    json.extract! document_store, :id, :name
    json.document_count document_store.documents.count
end
