require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title,         "Data Abstraction Assistant"
    assert_equal full_title("Help"), "Help | Data Abstraction Assistant"
  end
end
