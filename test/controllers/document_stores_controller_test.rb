require 'test_helper'

class DocumentStoresControllerTest < ActionController::TestCase
  setup do
    @document_store = document_stores(:one)
    log_in_as @document_store.user
  end

  test "should get index" do
    get :index, user_id: @document_store.user
    assert_response :success
    assert_not_nil assigns(:document_stores)
  end

  test "should get new" do
    get :new, user_id: @document_store.user
    assert_response :success
  end

  test "should create document_store" do
    assert_difference('DocumentStore.count') do
      post :create, document_store: { name: @document_store.name, user_id: @document_store.user_id }, user_id: @document_store.user
    end

    assert_redirected_to document_store_path(assigns(:document_store))
  end

  test "should show document_store" do
    get :show, id: @document_store
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @document_store
    assert_response :success
  end

  test "should redirect to login if not logged in" do
    patch :update, id: @document_store, document_store: { name: @document_store.name, user_id: @document_store.user_id }
    #assert_redirected_to document_store_path(assigns(:document_store))
    assert_redirected_to document_store_path(@document_store)
  end

  test "should update document_store" do
    patch :update, id: @document_store, document_store: { name: @document_store.name, user_id: @document_store.user_id }
    #assert_redirected_to document_store_path(assigns(:document_store))
    assert_redirected_to document_store_path(@document_store)
  end

  test "should destroy document_store" do
    assert_difference('DocumentStore.count', -1) do
      delete :destroy, id: @document_store
    end

    assert_redirected_to user_document_stores_path(@document_store.user)
  end
end
