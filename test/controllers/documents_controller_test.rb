require 'test_helper'

class DocumentsControllerTest < ActionController::TestCase
  setup do
    @document = documents(:one)
    log_in_as @document.document_store.user
  end

  test "should get index" do
    get :index, document_store_id: @document.document_store
    assert_response :success
    assert_not_nil assigns(:documents)
  end

  test "should get new" do
    get :new, document_store_id: @document.document_store
    assert_response :success
  end

  # test "should create document" do
  #   assert_difference('Document.count') do
  #     post :create, document: { document_store_id: @document.document_store_id, name: @document.name, file: @document.file }, document_store_id: @document.document_store
  #   end

  #   assert_redirected_to document_path(assigns(:document))
  # end

  # test "should show document" do
  #   get :show, id: @document
  #   assert_response :success
  # end

  test "should get edit" do
    get :edit, id: @document
    assert_response :success
  end

  test "should update document" do
    patch :update, id: @document, document: { document_store_id: @document.document_store_id, name: @document.name, file: @document.file }
    #assert_redirected_to document_path(assigns(:document))
    assert_redirected_to document_path(@document)
  end

  test "should destroy document" do
    assert_difference('Document.count', -1) do
      delete :destroy, id: @document
    end

    assert_redirected_to document_store_documents_path(@document.document_store)
  end
end
