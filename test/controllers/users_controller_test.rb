require 'test_helper'

class UsersControllerTest < ActionController::TestCase

  def setup
    @admin     = users(:jens)
    @non_admin = users(:michael)
  end

  test "should redirect index when not logged in" do
    get :index
    assert_redirected_to login_url
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should redirect edit when not logged in" do
    get :edit, id: @admin
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch :update, id: @admin, user: { name: @admin.name, email: @admin.email }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect edit when logged in as wrong user" do
    log_in_as(@non_admin)
    get :edit, id: @admin
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect update when logged in as wrong user" do
    log_in_as(@non_admin)
    patch :update, id: @admin, user: { name: @admin.name, email: @admin.email }
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should not allow the admin attribute to be edited via the web" do
    log_in_as(@non_admin)
    assert_not @non_admin.admin?
    patch :update, id: @non_admin, user: { password:              "password01",
                                           password_confirmation: "password01",
                                           admin: true }
    assert_not @non_admin.reload.admin?
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do
      delete :destroy, id: @admin
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when logged in as a non-admin" do
    log_in_as(@non_admin)
    assert_no_difference 'User.count' do
      delete :destroy, id: @admin
    end
    assert_redirected_to root_url
  end

  test "should redirect following when not logged in" do
    get :following, id: @admin
    assert_redirected_to login_url
  end

  test "should redirect followers when not logged in" do
    get :followers, id: @admin
    assert_redirected_to login_url
  end
end
