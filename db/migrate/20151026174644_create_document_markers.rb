class CreateDocumentMarkers < ActiveRecord::Migration
  def change
    create_table :document_markers do |t|
      t.references :document, index: true
      t.references :source, index: true
      t.text :text
      t.string :position

      t.timestamps null: false
    end
    add_foreign_key :document_markers, :documents
    add_foreign_key :document_markers, :sources
  end
end
