class CreateDocumentStores < ActiveRecord::Migration
  def change
    create_table :document_stores do |t|
      t.references :user, index: true
      t.string :name

      t.timestamps null: false
    end
    add_foreign_key :document_stores, :users
  end
end
