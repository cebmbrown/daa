class AddIndexToDocumentStores < ActiveRecord::Migration
  def change
    add_column :document_stores, :access_token, :string, null: false, default: ""
    add_index :document_stores, :access_token, unique: true
  end
end
