class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.references :document_store, index: true
      t.string :name
      t.string :file

      t.timestamps null: false
    end
    add_foreign_key :documents, :document_stores
  end
end
