class AddDeletedAtToDocumentStores < ActiveRecord::Migration
  def change
    add_column :document_stores, :deleted_at, :datetime
    add_index :document_stores, :deleted_at
  end
end
