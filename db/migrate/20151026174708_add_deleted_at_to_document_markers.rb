class AddDeletedAtToDocumentMarkers < ActiveRecord::Migration
  def change
    add_column :document_markers, :deleted_at, :datetime
    add_index :document_markers, :deleted_at
  end
end
