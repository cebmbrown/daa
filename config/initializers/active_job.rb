# Set the queue adapter for background jobs used by ActiveJobs
Rails.application.configure do
  config.active_job.queue_adapter = :inline
  # config.active_job.queue_adapter = :sidekiq
end
