# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Rails.application.initialize!

ActionMailer::Base.smtp_settings = {
    :address              => ENV['SMTP_HOST'],
    :port                 => ENV['SMTP_PORT'],
    :domain               => ENV['SMTP_DOMAIN'],
    :user_name            => ENV['SMTP_USERNAME'],
    :password             => ENV['SMTP_PASSWORD'],
    :authentication       => "plain",
    :enable_starttls_auto => true
}
